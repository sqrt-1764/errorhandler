/**
 * Prettyprint the error-messages produced by jQuery-Ajax-Calls and optionally
 * provide a callback to futher process error-messages.
 */
'use strict'

export default class ErrorHandler {

    constructor() {
        this.setErrorCallback(null);
    }

    /**
     * Optionales Setzen eines Callbacks, der im Fehlerfalle aufgerufen wird.
     * Tag nochmal separat übergeben, damit ggf. bequemer geloggt werden kann.
     *
     * @param {type} callback
     * @returns {Error}
     */
    setErrorCallback(callback) {
        this.errorCallback = (undefined === callback || null === callback)
                            ? function(tag, hdg, msg) { alert(hdg + '\n' + msg); }
                            : callback;
        return this;
    }

    /**
     * Prettyprint the error-details and forward to the error-callback for final processing.
     *
     * @param {type} tag
     * @param {type} hdg
     * @param {type} msg1
     * @param {type} xhr
     * @param {type} status
     * @param {type} error
     * @returns {undefined}
     */
    throwAjaxError(tag, hdg, msg1, xhr, status, error) {
        if (504 !== xhr.status && (0 !== xhr.status && 'error' !== status)) {
            let msg = tag + ': ';
            if (undefined !== msg1 && msg1.length > 0)   msg += 'Fehler: ' + msg1 + '\n';
            if (undefined !== status)                    msg += 'Status ' + status + ' ';
            if (undefined !== error && error.length > 0) msg += ' Errorcode ' + error + ' ';
            if (undefined !== xhr.status)                msg += ' Status (xhr) ' + xhr.status + ' ';
            if (undefined !== xhr.responseText)          msg += '\n' + xhr.responseText;

            this.errorCallback(tag, hdg, msg);      // Tag nochmal separat übergeben, damit ggf. bequemer geloggt werden kann
        }
    }
}
